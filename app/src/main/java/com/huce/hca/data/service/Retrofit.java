//package com.huce.hca.data.service;
//
//import android.content.Context;
//
//import com.huce.hca.common.service.HcaApiService;
//
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.logging.HttpLoggingInterceptor;
//import retrofit2.converter.gson.GsonConverterFactory;
//
//public class Retrofit {
//    public static Retrofit retrofit;
//
//    public Retrofit(){
//
//    }
//
//    public HcaApiService RequestAuth(Context ctx) {
//        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
//        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        OkHttpClient client =  new OkHttpClient.Builder()
//                .addInterceptor(httpLoggingInterceptor)
//                .addInterceptor(chain -> {
//                    Request newRequest  = chain.request().newBuilder()
//                            .addHeader("Authorization", "Bearer ")
//                            .build();
//                    return chain.proceed(newRequest);
//                })
//                .build();
//
//        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
//                .baseUrl(BuildConfig.API_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(client)
//                .build();
//        return retrofit.create(HcaApiService.class);
//    }
//
//    public HcaApiService Request() {
//        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
//                .baseUrl(BuildConfig.API_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        return retrofit.create(HcaApiService.class);
//    }
//
//    public static Retrofit getInstance() {
//        if (retrofit == null) {
//            retrofit = new Retrofit();
//        }
//        return retrofit;
//    }
//}
