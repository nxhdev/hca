package com.huce.hca.data.remote;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class HttpResponse<T> {
    private boolean success;
    private String message;
    private T data;
    public boolean getSuccess() {
        return success;
    }
}
