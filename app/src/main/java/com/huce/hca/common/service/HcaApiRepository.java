package com.huce.hca.common.service;

import com.huce.hca.data.remote.HttpResponse;
import com.huce.hca.presentation.app.author.dto.AuthorResponseDTO;
import com.huce.hca.presentation.app.book.dto.BookResponseDTO;
import com.huce.hca.presentation.app.category.dto.CategoryResponseDTO;
import com.huce.hca.presentation.app.chapter.Chapter;
import com.huce.hca.presentation.app.reading.ComicContent;
import com.huce.hca.presentation.app.review.dto.ReviewBookRequestDTO;
import com.huce.hca.presentation.app.review.dto.ReviewBookResponseDTO;
import com.huce.hca.presentation.login.dto.LoginRequestDTO;
import com.huce.hca.presentation.register.dto.RegisterRequestDTO;
import com.huce.hca.presentation.register.dto.RegisterResponseDTO;
import com.huce.hca.presentation.resetpassword.dto.ResetPasswordRequestDTO;
import com.huce.hca.presentation.resetpassword.dto.ResetPasswordResponseDTO;
import com.huce.hca.presentation.app.book.dto.BookSearchResultDTO;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HcaApiRepository {
        @POST("/api/auth/register")
        Call<HttpResponse<RegisterResponseDTO>> Register(@Body RegisterRequestDTO registerRequestDTO);
        @POST("/api/auth/login")
        Call<HttpResponse<String>> Login(@Body LoginRequestDTO loginRequestDTO);
        @POST("/api/auth/reset-password")
        Call<ResetPasswordResponseDTO> ResetPassword(@Body ResetPasswordRequestDTO resetPasswordRequestDTO);
        @POST("/api/auth/review_book")
        Call<ReviewBookResponseDTO> ReviewBook(@Body ReviewBookRequestDTO reviewBookRequestDTO);
        @GET("/api/comic/all_comic")
        Call<HttpResponse<List<BookResponseDTO>>> getBooks();
        @GET("/api/author/all")
        Call<HttpResponse<List<AuthorResponseDTO>>> getAuthors();
        @GET("/api/category/all")
        Call<HttpResponse<List<CategoryResponseDTO>>> getCategory();
        @GET("/api/comic/search")
        Call<HttpResponse<List<BookSearchResultDTO>>> SearchBooksByKeyword(@Query("keyword") String keyword);
        @GET("/api/comic/{comicId}")
        Call<HttpResponse<ReviewBookResponseDTO>> getComicById(@Path("comicId") long comicId);
        @GET("/api/chapter/{comicId}")
        Call<HttpResponse<List<Chapter>>> getChapter(@Path("comicId") long comicId);
        @GET("/api/comic/{comicId}/{chapterId}")
        Call<HttpResponse<ComicContent>> getContentByChapterId(@Path("comicId") long comicId, @Path("chapterId") long chapterId);
}
