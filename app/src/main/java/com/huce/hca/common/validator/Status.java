package com.huce.hca.common.validator;

public class Status {
    public static final int registerSuccess = 0;
    public static final int emptyEmail = 1;
    public static final int emptyUsername = 2;
    public static final int emptyPassword = 3;
    public static final int emptycfPassword = 4;
//    public static final int isEmail = 5;
    public static final int passwordMismatch = 5;
}
