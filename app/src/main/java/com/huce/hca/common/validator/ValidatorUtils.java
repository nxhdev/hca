package com.huce.hca.common.validator;

public class ValidatorUtils {
    public static boolean emptyValue(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static boolean isEmail(String email) {
        // Thực hiện kiểm tra định dạng email
        // Đây là một ví dụ đơn giản, bạn có thể thay thế bằng kiểm tra định dạng email phức tạp hơn
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
