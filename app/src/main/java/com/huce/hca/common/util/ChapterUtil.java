package com.huce.hca.common.util;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.huce.hca.R;
import com.huce.hca.presentation.app.chapter.Chapter;
import com.huce.hca.presentation.app.chapter.ChapterAdapter;
import com.huce.hca.presentation.app.reading.ReadingActivity;

import java.util.List;

public class ChapterUtil {
    public static void setupChapterSpinner(Spinner spinner, List<Chapter> chapters, Context context) {
        ChapterAdapter chapterAdapter = new ChapterAdapter(context, R.layout.chapter_selected, chapters);
        spinner.setAdapter(chapterAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                if (chapterAdapter != null) {
                    Chapter selectedChapter = chapterAdapter.getItem(i);
                    if (selectedChapter != null) {
                        Toast.makeText(context, selectedChapter.getTitle(), Toast.LENGTH_SHORT).show();
                        long selectedChapterId = selectedChapter.getId();
                        ((ReadingActivity) context).handleSelectedChapter(selectedChapterId);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
