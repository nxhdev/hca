package com.huce.hca.presentation.app.category;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.huce.hca.R;
import com.huce.hca.presentation.app.category.dto.CategoryResponseDTO;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private List<CategoryResponseDTO> categoryList;

    public CategoryAdapter(List<CategoryResponseDTO> categoryList) {
        this.categoryList = categoryList;
    }
    public void setData(List<CategoryResponseDTO> newcategoryList) {
        categoryList = newcategoryList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        CategoryResponseDTO category = categoryList.get(position);
        if (category == null){
            return;
        }
        holder.categoryTextView.setText(category.getName());
    }

    @Override
    public int getItemCount() {
        if (categoryList != null){
            return categoryList.size();
        }
        return 0;
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView categoryTextView;

        CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryTextView = itemView.findViewById(R.id.category);
        }
    }
}
