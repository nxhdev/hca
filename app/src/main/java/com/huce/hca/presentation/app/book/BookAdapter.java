package com.huce.hca.presentation.app.book;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.huce.hca.R;
import com.huce.hca.presentation.app.book.dto.BookResponseDTO;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    private static List<BookResponseDTO> bookList;

    public interface OnItemClickListener {
        void onItemClick(BookResponseDTO book);
    }

    private static OnItemClickListener itemClickListener;

    public BookAdapter(List<BookResponseDTO> bookList, OnItemClickListener itemClickListener) {
        BookAdapter.bookList = bookList;
        this.itemClickListener = itemClickListener;
    }

    public void setData(List<BookResponseDTO> newBookList) {
        bookList = newBookList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        BookResponseDTO bookResponseDTO = bookList.get(position);
        if (bookResponseDTO == null) {
            return;
        }
        Picasso.get().load(bookResponseDTO.getThumbnail()).into(holder.bookImageView);
        holder.bookName.setText(bookResponseDTO.getName());
    }

    @Override
    public int getItemCount() {
        if (bookList != null) {
            return bookList.size();
        }
        return 0;
    }

    public static class BookViewHolder extends RecyclerView.ViewHolder {

        private ImageView bookImageView;
        private TextView bookName;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            bookName = itemView.findViewById(R.id.tv_title);
            bookImageView = itemView.findViewById(R.id.img_book);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && itemClickListener != null) {
                        itemClickListener.onItemClick(bookList.get(position));
                    }
                }
            });
        }
    }

}
