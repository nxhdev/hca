package com.huce.hca.presentation.app.library;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.huce.hca.R;
import com.huce.hca.presentation.app.home.HomeActivity;
import com.huce.hca.presentation.app.home.HomeViewModel;
import com.huce.hca.presentation.app.book.BookAdapter;
import com.huce.hca.presentation.app.book.dto.BookResponseDTO;
import com.huce.hca.presentation.app.review.ReviewBookActivity;
import com.huce.hca.presentation.app.search.SearchActivity;
import com.huce.hca.presentation.login.LoginActivity;

import java.util.ArrayList;
import java.util.List;

public class LibraryActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BookAdapter.OnItemClickListener {
    private RecyclerView recentBook;
    private BookAdapter recentBookAdapter;
    private HomeViewModel homeViewModel;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_library);

        drawerLayout = findViewById(R.id.library_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);

        // toolbar
        setSupportActionBar(toolbar);

        // Navigation Drawer Menu
        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_open, R.string.navigation_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_library);

        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        setupRecyclerView();
        setupViewModel();
        observeViewModel();
    }

    private void observeViewModel() {
        homeViewModel.getBookList().observe(this, this::updateRecentBookList);
        homeViewModel.isLoading().observe(this, this::updateLoadingState);
        homeViewModel.getError().observe(this, this::showError);
    }

    private void showError(String errorMessage) {
        // Handle error message display
    }

    private void updateLoadingState(Boolean isLoading) {
        // Update UI based on loading state
    }

    private void updateRecentBookList(List<BookResponseDTO> recentBookList) {
        recentBookAdapter.setData(recentBookList);
        recentBookAdapter.notifyDataSetChanged();
    }
    private void setupViewModel() {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.fetchBooks();
    }

    private void setupRecyclerView() {
        recentBook = findViewById(R.id.rv_recent_book);
        int numberOfColumns = 2;
        GridLayoutManager layoutManager = new GridLayoutManager(this, numberOfColumns);
        recentBook.setLayoutManager(layoutManager);

        List<BookResponseDTO> recentBookList = new ArrayList<>();
        recentBookAdapter = new BookAdapter(recentBookList, this);
        recentBook.setAdapter(recentBookAdapter);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_library:
                break;
            case R.id.nav_home:
                Intent search = new Intent(LibraryActivity.this, HomeActivity.class);
                startActivity(search);
                break;
            case R.id.nav_search:
                Intent library = new Intent(LibraryActivity.this, SearchActivity.class);
                startActivity(library);
                break;
            case R.id.nav_logout:
                Intent logout = new Intent(LibraryActivity.this, LoginActivity.class);
                startActivity(logout);
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onItemClick(BookResponseDTO book) {
        Intent intent = new Intent(this, ReviewBookActivity.class);
        intent.putExtra("bookId", book.getId());
        intent.putExtra("img", book.getThumbnail());
        intent.putExtra("description", book.getDescription());
        startActivity(intent);
    }
}
