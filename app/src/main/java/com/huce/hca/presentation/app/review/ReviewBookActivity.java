package com.huce.hca.presentation.app.review;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.huce.hca.R;
import com.huce.hca.databinding.ActivityReviewBookBinding;
import com.huce.hca.presentation.app.choice_chapter.ChoiceChapterActivity;
import com.huce.hca.presentation.app.home.HomeActivity;
import com.huce.hca.presentation.app.review.dto.ReviewBookResponseDTO;
import com.squareup.picasso.Picasso;

public class ReviewBookActivity extends AppCompatActivity {
    private ActivityReviewBookBinding binding;
    private ReviewBookViewModel reviewBookViewModel;
    private ReviewBookResponseDTO reviewBookResponseDTO;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        reviewBookViewModel = new ViewModelProvider(this).get(ReviewBookViewModel.class);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_review_book);

        binding.setLifecycleOwner(this);

        binding.setReviewBookViewModel(reviewBookViewModel);

        Button read_now = binding.btnReading;
        Button back_button = binding.backButton;
        ImageView imageView2 = binding.imageView2;
        TextView textViewDescription = binding.review;

        Intent intent = getIntent();
        String img = intent.getStringExtra("img");
        Picasso.get().load(img).into(imageView2);


        if (intent != null && intent.hasExtra("description")) {
            String description = intent.getStringExtra("description");
            textViewDescription.setText(description);
        }
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        read_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReviewBookActivity.this, ChoiceChapterActivity.class);
                intent.putExtra("img", getIntent().getStringExtra("img"));
                intent.putExtra("bookId", getIntent().getLongExtra("bookId", 0));
                startActivity(intent);
            }
        });
    }
}

