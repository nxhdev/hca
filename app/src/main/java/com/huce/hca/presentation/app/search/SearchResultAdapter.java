package com.huce.hca.presentation.app.search;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.huce.hca.R;
import com.huce.hca.presentation.app.book.dto.BookSearchResultDTO;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.BookViewHolder> {
    private List<BookSearchResultDTO> bookList;
    public SearchResultAdapter() {
        this.bookList = new ArrayList<>();
    }

    public void setData(List<BookSearchResultDTO> newData) {
        this.bookList = newData;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.component_search_result, parent, false);
        return new BookViewHolder(view);
    }
    public interface OnItemClickListener {
        void onItemClick(BookSearchResultDTO book);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        BookSearchResultDTO book = bookList.get(position);
        Picasso.get().load(book.getThumbnail()).into(holder.imgBook);
        holder.tvTitle.setText(book.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(book);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public static class BookViewHolder extends RecyclerView.ViewHolder {
        ImageView imgBook;
        TextView tvTitle;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            imgBook = itemView.findViewById(R.id.img_book_result);
            tvTitle = itemView.findViewById(R.id.tv_book_result_Name);
        }
    }
}