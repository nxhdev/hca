package com.huce.hca.presentation.app.reading;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ComicContent {
    private Long id;
    private Long chapter;
    private Long comic;
    private String content;
}
