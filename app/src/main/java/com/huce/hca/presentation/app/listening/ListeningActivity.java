package com.huce.hca.presentation.app.listening;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.huce.hca.R;
import com.huce.hca.presentation.app.choice_chapter.ChoiceChapterActivity;

public class ListeningActivity extends AppCompatActivity {
    private Button btn_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_listenning);

        btn_back = findViewById(R.id.backButton);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(ListeningActivity.this, ChoiceChapterActivity.class);
                startActivity(back);
            }
        });
    }
}
