package com.huce.hca.presentation.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.huce.hca.R;
import com.huce.hca.databinding.ActivityGuestRegisterBinding;
import com.huce.hca.presentation.app.welcome.WelcomeActivity;
import com.huce.hca.presentation.login.LoginActivity;

public class RegisterActivity extends AppCompatActivity {
    private ActivityGuestRegisterBinding binding;
    private RegisterViewModel registerViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_guest_register);

        binding.setLifecycleOwner(this);

        binding.setRegisterViewModel(registerViewModel);

        registerViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);

        Button buttonSignUp = binding.btnLoginWithAccount;
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        registerViewModel.registerResponse.observe(this, resp -> {
            if (resp.getSuccess()) {
                Toast.makeText(RegisterActivity.this, "Success: " + resp.getMessage(), Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(RegisterActivity.this, "Error: " + resp.getMessage(), Toast.LENGTH_SHORT).show();
                handleRegistrationSuccess();
            }
        });
    }

    private void handleRegistrationSuccess() {
        Toast.makeText(this, "Registration successful!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(RegisterActivity.this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }
}

