package com.huce.hca.presentation.register;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.huce.hca.common.service.RetrofitService;
import com.huce.hca.common.validator.Status;
import com.huce.hca.common.validator.ValidatorUtils;
import com.huce.hca.data.remote.HttpResponse;
import com.huce.hca.presentation.register.dto.RegisterRequestDTO;
import com.huce.hca.presentation.register.dto.RegisterResponseDTO;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterViewModel extends ViewModel {
    public RegisterRequestDTO user = new RegisterRequestDTO();
    public MutableLiveData<Integer> getRegisterStatus() {
        return RegisterStatus;
    }
    private MutableLiveData<Integer> RegisterStatus = new MutableLiveData<>();
    public MutableLiveData<HttpResponse<RegisterResponseDTO>> registerResponse = new MutableLiveData<HttpResponse<RegisterResponseDTO>>();
    public String confirmPassword;
    public void registerHandler() {
        ValidationStatus validationStatus = validateData();
        if (validationStatus == ValidationStatus.SUCCESS) {
            if (user.getPassword() == null || !user.getPassword().equals(confirmPassword)) {
                HttpResponse<RegisterResponseDTO> res = new HttpResponse<>();
                res.setMessage("Mật khẩu xác nhận không khớp");
                registerResponse.setValue(res);
                return;
            }
            Call<HttpResponse<RegisterResponseDTO>> repos = RetrofitService.getInstance().Request().Register(user);

            repos.enqueue(new Callback<HttpResponse<RegisterResponseDTO>>() {
                @Override
                public void onResponse(Call<HttpResponse<RegisterResponseDTO>> call, Response<HttpResponse<RegisterResponseDTO>> response) {
                    if (!response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            HttpResponse<RegisterResponseDTO> res = gson.fromJson(response.errorBody().charStream(), HttpResponse.class);
                            registerResponse.setValue(res);
                        } catch (Exception e) {
                            HttpResponse<RegisterResponseDTO> res = new HttpResponse<>();
                            res.setMessage("Đăng ký thất bại, Lỗi: " + e.getMessage());
                            registerResponse.setValue(res);
                        }
                        return;
                    }
                    HttpResponse<RegisterResponseDTO> post = response.body();
                    registerResponse.setValue(post);
                }

                @Override
                public void onFailure(Call<HttpResponse<RegisterResponseDTO>> call, Throwable t) {
                    // Xử lý khi request thất bại
                    Log.d("Error", "Thất bại!. Lỗi: " + t.getMessage());
                    HttpResponse<RegisterResponseDTO> res = new HttpResponse<>();
                    res.setMessage("Kết nối đến máy chủ thất bại.");
                    registerResponse.setValue(res);
                }
            });
        } else {
            handleValidationFailure(validationStatus);
        }
    }

    public enum ValidationStatus {
        SUCCESS,
        EMPTY_NAME,
        EMPTY_EMAIL,
        EMPTY_USERNAME,
        EMPTY_PASSWORD,
        EMPTY_CONFIRM_PASSWORD,
        INVALID_EMAIL,
        PASSWORD_MISMATCH
    }

    // Hàm kiểm tra dữ liệu nhập
    public ValidationStatus validateData() {
        if (ValidatorUtils.emptyValue(user.getName())) {
            return ValidationStatus.EMPTY_NAME;
        } else if (ValidatorUtils.emptyValue(user.getEmail())) {
            return ValidationStatus.EMPTY_EMAIL;
        } else if (ValidatorUtils.emptyValue(user.getUsername())) {
            return ValidationStatus.EMPTY_USERNAME;
        } else if (ValidatorUtils.emptyValue(user.getPassword())) {
            return ValidationStatus.EMPTY_PASSWORD;
        } else if (ValidatorUtils.emptyValue(confirmPassword)) {
            return ValidationStatus.EMPTY_CONFIRM_PASSWORD;
        } else if (!ValidatorUtils.isEmail(user.getEmail())) {
            return ValidationStatus.INVALID_EMAIL;
        } else if (!user.getPassword().equals(confirmPassword)) {
            return ValidationStatus.PASSWORD_MISMATCH;
        } else {
            return ValidationStatus.SUCCESS;
        }
    }
    private void handleValidationFailure(ValidationStatus validationStatus) {
        switch (validationStatus) {
            case EMPTY_EMAIL:
                RegisterStatus.setValue(Status.emptyEmail);
                break;
            case EMPTY_USERNAME:
                RegisterStatus.setValue(Status.emptyUsername);
                break;
            case EMPTY_PASSWORD:
                RegisterStatus.setValue(Status.emptyPassword);
                break;
            case EMPTY_CONFIRM_PASSWORD:
                RegisterStatus.setValue(Status.emptycfPassword);
                break;
            case PASSWORD_MISMATCH:
                RegisterStatus.setValue(Status.passwordMismatch);
                break;
        }
    }
}
