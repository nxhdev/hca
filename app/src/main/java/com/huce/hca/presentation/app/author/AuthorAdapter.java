package com.huce.hca.presentation.app.author;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.huce.hca.R;
import com.huce.hca.presentation.app.author.dto.AuthorResponseDTO;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.AuthorViewHolder> {

    private List<AuthorResponseDTO> authorList;

    public AuthorAdapter(List<AuthorResponseDTO> authorList) {
        this.authorList = authorList;
    }
    public void setData(List<AuthorResponseDTO> newAuthorList) {
        authorList = newAuthorList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public AuthorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_author, parent, false);
        return new AuthorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AuthorViewHolder holder, int position) {
        AuthorResponseDTO author = authorList.get(position);
        if (author == null){
            return;
        }
        Picasso.get().load(author.getThumbnail()).into(holder.img_author);
        holder.author_name.setText(author.getName());
    }

    @Override
    public int getItemCount() {
        if (authorList != null){
            return authorList.size();
        }
        return 0;
    }

    static class AuthorViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView img_author;
        private TextView author_name;

        public AuthorViewHolder(@NonNull View itemView) {
            super(itemView);
            img_author = itemView.findViewById(R.id.img_author);
            author_name = itemView.findViewById(R.id.tv_author_name);
        }
    }
}
