package com.huce.hca.presentation.app.choice_chapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.huce.hca.R;
import com.huce.hca.common.service.HcaApiRepository;
import com.huce.hca.common.service.RetrofitService;
import com.huce.hca.data.remote.HttpResponse;
import com.huce.hca.presentation.app.chapter.Chapter;
import com.huce.hca.presentation.app.chapter.ChapterAdapter;
import com.huce.hca.presentation.app.reading.ReadingActivity;

import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChoiceChapterActivity extends AppCompatActivity {
    private Spinner spnChapter;
    private ChapterAdapter chapterAdapter;
    private Button btn_back;
    private View btn_reading;

    private int selectedChapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_chapter);
        btn_back = findViewById(R.id.back_button);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btn_reading = findViewById(R.id.btnReadNow);
        btn_reading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent onReading = new Intent(ChoiceChapterActivity.this, ReadingActivity.class);
                onReading.putExtra("chapterId", selectedChapter);
                onReading.putExtra("bookId", getIntent().getLongExtra("bookId", 0));
                onReading.putExtra("callChapter", true);
                startActivity(onReading);
            }
        });
        callChapter(getIntent().getLongExtra("bookId", 0));
    }
    public void callChapter(Long comicId){
        HcaApiRepository apiRepository = RetrofitService.getInstance().Request();

        Call<HttpResponse<List<Chapter>>> call = apiRepository.getChapter(comicId);
        call.enqueue(new Callback<HttpResponse<List<Chapter>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<Chapter>>> call, Response<HttpResponse<List<Chapter>>> response) {
                if (response.isSuccessful()) {
                    HttpResponse<List<Chapter>> httpResponse = response.body();
                    if (httpResponse != null && httpResponse.getData() != null) {
                        List<Chapter> chapters = httpResponse.getData();

                        int totalChapters = chapters.size();
                        TextView txtTotalChapter = findViewById(R.id.txt_totalChapter);
                        txtTotalChapter.setText(String.valueOf(totalChapters));
                        setupSpinner(chapters);
                    }
                } else {
                    Toast.makeText(ChoiceChapterActivity.this, "Failed to fetch chapters", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<HttpResponse<List<Chapter>>> call, Throwable t) {
                // Handle failure
                Toast.makeText(ChoiceChapterActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void setupSpinner(List<Chapter> chapters) {
        spnChapter = findViewById(R.id.spn_chapter_category);
        chapterAdapter = new ChapterAdapter(this, R.layout.chapter_selected, chapters);
        spnChapter.setAdapter(chapterAdapter);
        spnChapter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                if (chapterAdapter != null) {
                    Toast.makeText(ChoiceChapterActivity.this, chapterAdapter.getItem(i).getTitle(), Toast.LENGTH_SHORT).show();
                    selectedChapter = Objects.requireNonNull(chapterAdapter.getItem(i)).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

}
