package com.huce.hca.presentation.resetpassword;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.huce.hca.R;
import com.huce.hca.databinding.ActivityGuestResetpasswordBinding;
import com.huce.hca.presentation.app.review.ReviewBookActivity;
import com.huce.hca.presentation.login.LoginActivity;

public class ResetPasswordActivity extends AppCompatActivity {
    private ActivityGuestResetpasswordBinding binding;
    private ResetPasswordViewModel resetPasswordViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        resetPasswordViewModel = new ViewModelProvider(this).get(ResetPasswordViewModel.class);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_guest_resetpassword);

        binding.setLifecycleOwner(this);

        binding.setResetPasswordViewModel(resetPasswordViewModel);


        Button buttonCancel = binding.cancelButton;
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


    }
}
