package com.huce.hca.presentation.login;

import android.util.Log;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.huce.hca.common.service.RetrofitService;
import com.huce.hca.data.remote.HttpResponse;
import com.huce.hca.presentation.login.dto.LoginRequestDTO;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {
    public LoginRequestDTO user = new LoginRequestDTO();
    public ObservableField<String> username = new ObservableField<>();

    // Phương thức để lấy giá trị username
    public String getUsername() {
        return username.get();
    }
    public MutableLiveData<HttpResponse<String>> loginResponse = new MutableLiveData<HttpResponse<String>>();
    public void loginHandler() {
        Call<HttpResponse<String>> repos = RetrofitService.getInstance().Request().Login(user);
        repos.enqueue(new Callback<HttpResponse<String>>() {
            @Override
            public void onResponse(Call<HttpResponse<String>> call, Response<HttpResponse<String>> response) {
                if (!response.isSuccessful()) {
                    try {
                        Gson gson = new Gson();
                        HttpResponse<String> res = gson.fromJson(response.errorBody().charStream(), HttpResponse.class);
                        loginResponse.setValue(res);
                    } catch (Exception e) {
                        HttpResponse<String> res = new HttpResponse<>();
                        res.setMessage("Đăng nhập thất bại, Lỗi: " + e.getMessage());
                        loginResponse.setValue(res);
                    }
                    return;
                }
                HttpResponse<String> post = response.body();
                loginResponse.setValue(post);
            }
            @Override
            public void onFailure(Call<HttpResponse<String>> call, Throwable t) {
                // Xử lý khi request thất bại
                Log.d("Error", "Thất bại!. Lỗi: " + t.getMessage());
                HttpResponse<String> res = new HttpResponse<>();
                res.setMessage("Thất bại");
                loginResponse.setValue(res);
            }
        });

    }

}
