package com.huce.hca.presentation.app.chapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huce.hca.R;

import java.util.List;

public class ChapterAdapter extends ArrayAdapter<Chapter> {

    private List<Chapter> chapters;

    public ChapterAdapter(@NonNull Context context, int resource, @NonNull List<Chapter> objects) {
        super(context, resource, objects);
        this.chapters = objects;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_selected, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvSelected = convertView.findViewById(R.id.tv_selected);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Chapter chapter = getItem(position);
        if (chapter != null) {
            viewHolder.tvSelected.setText(chapter.getTitle());
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_category, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvChapter = convertView.findViewById(R.id.tv_chapter_id);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Chapter chapter = getItem(position);
        if (chapter != null) {
            viewHolder.tvChapter.setText(chapter.getTitle());
        }

        return convertView;
    }

    static class ViewHolder {
        TextView tvSelected;
        TextView tvChapter;
    }
}
