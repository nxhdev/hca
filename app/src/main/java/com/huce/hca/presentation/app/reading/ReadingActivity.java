package com.huce.hca.presentation.app.reading;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.huce.hca.R;
import com.huce.hca.common.service.HcaApiRepository;
import com.huce.hca.common.service.RetrofitService;
import com.huce.hca.common.util.ChapterUtil;
import com.huce.hca.data.remote.HttpResponse;
import com.huce.hca.presentation.app.chapter.Chapter;
import com.huce.hca.presentation.app.chapter.ChapterAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReadingActivity extends AppCompatActivity {
    private Button btn_back;
    private Spinner spnChapter;
    private ChapterAdapter chapterAdapter;
    private TextView txtContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_reading);
        txtContent = findViewById(R.id.txtContent);
        btn_back = findViewById(R.id.backButton);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        int chapterId = intent.getIntExtra("chapterId", 1);
        callChapter(getIntent().getLongExtra("bookId", 0));
    }

    private void setupSpinner(List<Chapter> chapters) {
        spnChapter = findViewById(R.id.spn_chapter_category);
        ChapterUtil.setupChapterSpinner(spnChapter, chapters, this);
    }

    private void loadData(long bookId, long chapterId) {
        HcaApiRepository apiRepository = RetrofitService.getInstance().Request();
        Call<HttpResponse<ComicContent>> call = apiRepository.getContentByChapterId(bookId, chapterId);
        call.enqueue(new Callback<HttpResponse<ComicContent>>() {
            @Override
            public void onResponse(Call<HttpResponse<ComicContent>> call, Response<HttpResponse<ComicContent>> response) {
                if (response.isSuccessful()) {
                    HttpResponse<ComicContent> httpResponse = response.body();
                    if (httpResponse != null && httpResponse.getData() != null) {
                        txtContent.setText(response.body().getData().getContent());
                    } else {
                        Toast.makeText(ReadingActivity.this, "Failed to fetch chapters", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<HttpResponse<ComicContent>> call, Throwable t) {
                Toast.makeText(ReadingActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void handleSelectedChapter(long selectedChapterId) {
        loadData(getIntent().getLongExtra("bookId", 1), selectedChapterId);
    }

    public void callChapter(Long comicId){
        HcaApiRepository apiRepository = RetrofitService.getInstance().Request();

        Call<HttpResponse<List<Chapter>>> call = apiRepository.getChapter(comicId);
        call.enqueue(new Callback<HttpResponse<List<Chapter>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<Chapter>>> call, Response<HttpResponse<List<Chapter>>> response) {
                if (response.isSuccessful()) {
                    HttpResponse<List<Chapter>> httpResponse = response.body();
                    if (httpResponse != null && httpResponse.getData() != null) {
                        List<Chapter> chapters = httpResponse.getData();
                        setupSpinner(chapters);
                    }
                } else {
                    Toast.makeText(ReadingActivity.this, "Failed to fetch chapters", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HttpResponse<List<Chapter>>> call, Throwable t) {
                // Handle failure
                Toast.makeText(ReadingActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
