package com.huce.hca.presentation.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.huce.hca.R;
import com.huce.hca.presentation.login.LoginActivity;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Delay for a few seconds and then open the main activity
        Thread splashThread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2000); // Delay for 2 seconds
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    // Start the main activity
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        splashThread.start();
    }
}
