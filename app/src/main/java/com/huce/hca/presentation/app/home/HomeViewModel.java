package com.huce.hca.presentation.app.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.huce.hca.common.service.HcaApiRepository;
import com.huce.hca.common.service.RetrofitService;
import com.huce.hca.data.remote.HttpResponse;
import com.huce.hca.presentation.app.author.dto.AuthorResponseDTO;
import com.huce.hca.presentation.app.book.dto.BookResponseDTO;
import com.huce.hca.presentation.app.category.dto.CategoryResponseDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {
    private final MutableLiveData<List<BookResponseDTO>> bookListLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<BookResponseDTO>> bookFavouriteListLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<AuthorResponseDTO>> authorListLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<CategoryResponseDTO>> categoryListLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loadingLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> errorLiveData = new MutableLiveData<>();
    public LiveData<List<BookResponseDTO>> getBookList() {
        return bookListLiveData;
    }
    public LiveData<List<AuthorResponseDTO>> getAuthorList() {
        return authorListLiveData;
    }
    public MutableLiveData<List<CategoryResponseDTO>> getCategoryList() {
        return categoryListLiveData;
    }
    public LiveData<Boolean> isLoading() {
        return loadingLiveData;
    }
    public LiveData<String> getError() {
        return errorLiveData;
    }
    public void fetchBooks() {
        loadingLiveData.setValue(true);

        HcaApiRepository apiRepository = RetrofitService.getInstance().Request();

        Call<HttpResponse<List<BookResponseDTO>>> call = apiRepository.getBooks();

        call.enqueue(new Callback<HttpResponse<List<BookResponseDTO>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<BookResponseDTO>>> call, Response<HttpResponse<List<BookResponseDTO>>> response) {
                loadingLiveData.setValue(false);
                if (response.isSuccessful()) {
                    bookListLiveData.setValue(response.body().getData());
                } else {
                    errorLiveData.setValue("Error fetching books");
                }
            }

            @Override
            public void onFailure(Call<HttpResponse<List<BookResponseDTO>>> call, Throwable t) {
                loadingLiveData.setValue(false);
                errorLiveData.setValue("Network error");
            }
        });
    }
    public void fetchAuthors() {
        loadingLiveData.setValue(true);

        HcaApiRepository apiRepository = RetrofitService.getInstance().Request();

        Call<HttpResponse<List<AuthorResponseDTO>>> call = apiRepository.getAuthors();

        call.enqueue(new Callback<HttpResponse<List<AuthorResponseDTO>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<AuthorResponseDTO>>> call, Response<HttpResponse<List<AuthorResponseDTO>>> response) {
                loadingLiveData.setValue(false);
                if (response.isSuccessful()) {
                    authorListLiveData.setValue(response.body().getData());
                } else {
                    errorLiveData.setValue("Error fetching books");
                }
            }

            @Override
            public void onFailure(Call<HttpResponse<List<AuthorResponseDTO>>> call, Throwable t) {
                loadingLiveData.setValue(false);
                errorLiveData.setValue("Network error");
            }
        });
    }

    public void fetchCategories() {
        loadingLiveData.setValue(true);

        HcaApiRepository apiRepository = RetrofitService.getInstance().Request();

        Call<HttpResponse<List<CategoryResponseDTO>>> call = apiRepository.getCategory();

        call.enqueue(new Callback<HttpResponse<List<CategoryResponseDTO>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<CategoryResponseDTO>>> call, Response<HttpResponse<List<CategoryResponseDTO>>> response) {
                loadingLiveData.setValue(false);
                if (response.isSuccessful()) {
                    categoryListLiveData.setValue(response.body().getData());
                } else {
                    errorLiveData.setValue("Error fetching books");
                }
            }

            @Override
            public void onFailure(Call<HttpResponse<List<CategoryResponseDTO>>> call, Throwable t) {
                loadingLiveData.setValue(false);
                errorLiveData.setValue("Network error");
            }
        });
    }
}

