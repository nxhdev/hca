package com.huce.hca.presentation.app.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.huce.hca.R;
import com.huce.hca.presentation.app.author.AuthorAdapter;
import com.huce.hca.presentation.app.author.dto.AuthorResponseDTO;
import com.huce.hca.presentation.app.book.BookAdapter;
import com.huce.hca.presentation.app.book.dto.BookResponseDTO;
import com.huce.hca.presentation.app.category.CategoryAdapter;
import com.huce.hca.presentation.app.category.dto.CategoryResponseDTO;
import com.huce.hca.presentation.app.library.LibraryActivity;
import com.huce.hca.presentation.app.review.ReviewBookActivity;
import com.huce.hca.presentation.app.search.SearchActivity;
import com.huce.hca.presentation.login.LoginActivity;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BookAdapter.OnItemClickListener {
    private RecyclerView authorRecyclerView, bookRecyclerView, categoryRecyclerView;
    private AuthorAdapter authorAdapter;
    private BookAdapter bookAdapter;
    private CategoryAdapter categoryAdapter;
    private HomeViewModel homeViewModel;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_home:
                break;
            case R.id.nav_search:
                Intent search = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(search);
                break;
            case R.id.nav_library:
                Intent library = new Intent(HomeActivity.this, LibraryActivity.class);
                startActivity(library);
                break;
            case  R.id.nav_logout:
                Intent logout = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(logout);
                finish();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        // toolbar
        setSupportActionBar(toolbar);
        //Navigation Drawer Menu
        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar,R.string.navigation_open,R.string.navigation_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);


        Intent intent = getIntent();
        String username = intent.getStringExtra("USERNAME");
        TextView tvHello = findViewById(R.id.tvHello);
        tvHello.setText("Xin Chào " + username + "!");

        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        setupRecyclerView();
        setupViewModel();
        observeViewModel();
    }
    private void setupRecyclerView() {
        bookRecyclerView = findViewById(R.id.rv_book1_activity_home);
        LinearLayoutManager bookLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        bookRecyclerView.setLayoutManager(bookLayoutManager);
        List<BookResponseDTO> bookList = new ArrayList<>();
        bookAdapter = new BookAdapter(new ArrayList<>(), this);
        bookRecyclerView.setAdapter(bookAdapter);

        authorRecyclerView = findViewById(R.id.rv_author_activity_home);
        LinearLayoutManager authorLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        authorRecyclerView.setLayoutManager(authorLayoutManager);
        authorAdapter = new AuthorAdapter(new ArrayList<>());
        authorRecyclerView.setAdapter(authorAdapter);

        categoryRecyclerView = findViewById(R.id.rv_category_activity_home);
        LinearLayoutManager categoryLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        categoryRecyclerView.setLayoutManager(categoryLayoutManager);
        categoryAdapter = new CategoryAdapter(new ArrayList<>());
        categoryRecyclerView.setAdapter(categoryAdapter);
    }

    private void setupViewModel() {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.fetchBooks();
        homeViewModel.fetchAuthors();
        homeViewModel.fetchCategories();
    }

    private void observeViewModel() {
        homeViewModel.getBookList().observe(this, this::updateBookList);
        homeViewModel.getAuthorList().observe(this, this::updateAuthorList);
        homeViewModel.getCategoryList().observe(this, this::updateCategoryList);
        homeViewModel.isLoading().observe(this, this::updateLoadingState);
        homeViewModel.getError().observe(this, this::showError);
    }

    private void updateBookList(List<BookResponseDTO> bookList) {
        bookAdapter.setData(bookList);
        bookAdapter.notifyDataSetChanged();
    }
    @Override
    public void onItemClick(BookResponseDTO book) {
        Intent intent = new Intent(this, ReviewBookActivity.class);
        long bookId = book.getId();
        intent.putExtra("bookId", bookId);
        intent.putExtra("img", book.getThumbnail());
        intent.putExtra("description", book.getDescription());
        startActivity(intent);
    }
    private void updateAuthorList(List<AuthorResponseDTO> authorList) {
        authorAdapter.setData(authorList);
        authorAdapter.notifyDataSetChanged();
    }

    private void updateCategoryList(List<CategoryResponseDTO> categoryList) {
        categoryAdapter.setData(categoryList);
        categoryAdapter.notifyDataSetChanged();
    }

    private void updateLoadingState(Boolean isLoading) {
    }

    private void showError(String errorMessage) {
    }



    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }
}