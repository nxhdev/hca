package com.huce.hca.presentation.app.search;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.huce.hca.R;
import com.huce.hca.common.service.RetrofitService;
import com.huce.hca.data.remote.HttpResponse;
import com.huce.hca.presentation.app.book.dto.BookSearchResultDTO;
import com.huce.hca.presentation.app.home.HomeActivity;
import com.huce.hca.presentation.app.library.LibraryActivity;
import com.huce.hca.presentation.app.review.ReviewBookActivity;
import com.huce.hca.presentation.login.LoginActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private EditText searchEditText;
    private RecyclerView recyclerView;
    private SearchResultAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_search);

        drawerLayout = findViewById(R.id.search_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.list_result);
        searchEditText = findViewById(R.id.edt_search);

        setSupportActionBar(toolbar);
        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_open, R.string.navigation_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_search);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SearchResultAdapter();
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new SearchResultAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BookSearchResultDTO book) {
                Intent intent = new Intent(SearchActivity.this, ReviewBookActivity.class);
                long bookId = book.getId();
                intent.putExtra("bookId", bookId);
                intent.putExtra("img", book.getThumbnail());
                intent.putExtra("description", book.getDescription());
                startActivity(intent);
            }
        });
        searchEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                    getData(searchEditText.getText().toString());

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                getData("");
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }
    private void getData(String keyword) {
        Call<HttpResponse<List<BookSearchResultDTO>>> repos = RetrofitService.getInstance().Request().SearchBooksByKeyword(keyword);

        repos.enqueue(new Callback<HttpResponse<List<BookSearchResultDTO>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<BookSearchResultDTO>>> call, Response<HttpResponse<List<BookSearchResultDTO>>> response) {
                if (response.isSuccessful()) {
                    HttpResponse<List<BookSearchResultDTO>> responseData = response.body();
                    List<BookSearchResultDTO> books = responseData.getData();
                    adapter.setData(books);
                } else {
                    Log.e("SearchActivity", "Không thể lấy kết quả tìm kiếm. Mã: " + response.code());
                }
            }
            @Override
            public void onFailure(Call<HttpResponse<List<BookSearchResultDTO>>> call, Throwable t) {
                Log.e("SearchActivity", "Yêu cầu tìm kiếm thất bại. Lỗi: " + t.getMessage());
            }
        });
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_search:
                break;
            case R.id.nav_home:
                startActivity(new Intent(SearchActivity.this, HomeActivity.class));
                break;
            case R.id.nav_library:
                startActivity(new Intent(SearchActivity.this, LibraryActivity.class));
                break;
            case R.id.nav_logout:
                startActivity(new Intent(SearchActivity.this, LoginActivity.class));
                finish();
                break;
        }
        return true;
    }
}
