package com.huce.hca.presentation.app.review.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ReviewBookResponseDTO {
    private int id;
    private String name;
    private String message;
    private String thumbnail;
    private boolean success;
}