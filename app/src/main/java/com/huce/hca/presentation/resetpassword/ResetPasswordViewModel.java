package com.huce.hca.presentation.resetpassword;

import android.util.Log;

import androidx.lifecycle.ViewModel;
import com.huce.hca.presentation.resetpassword.dto.ResetPasswordRequestDTO;
import com.huce.hca.presentation.resetpassword.dto.ResetPasswordResponseDTO;
import com.huce.hca.common.service.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordViewModel extends ViewModel {

    public ResetPasswordRequestDTO user = new ResetPasswordRequestDTO();

    public void resetPasswordHandler() {
        Call<ResetPasswordResponseDTO> repos = RetrofitService.getInstance().Request().ResetPassword(user);

        repos.enqueue(new Callback<ResetPasswordResponseDTO>() {
            @Override
            public void onResponse(Call<ResetPasswordResponseDTO> call, Response<ResetPasswordResponseDTO> response) {
                if (!response.isSuccessful()) {
                    Log.d("Error", "Thất bại!");
                    return;
                }

                ResetPasswordResponseDTO post = response.body();
                post.getMessage();
            }

            @Override
            public void onFailure(Call<ResetPasswordResponseDTO> call, Throwable t) {
                // Xử lý khi request thất bại
                Log.d("Error", "Thất bại!");
            }
        });
    }
}
