package com.huce.hca.presentation.resetpassword.dto;

public class ResetPasswordRequestDTO {
    private String email;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
