package com.huce.hca.presentation.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.huce.hca.R;
import com.huce.hca.databinding.ActivityGuestLoginBinding;
import com.huce.hca.presentation.app.home.HomeActivity;
import com.huce.hca.presentation.register.RegisterActivity;
import com.huce.hca.presentation.resetpassword.ResetPasswordActivity;

public class LoginActivity extends AppCompatActivity {
    private ActivityGuestLoginBinding binding;
    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        binding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_guest_login);

        binding.setLifecycleOwner(this);

        binding.setLoginViewModel(loginViewModel);

        Button buttonSignUp = binding.button3;
        TextView buttonForget = binding.buttonToFogetPassword;
        TextView Login = binding.welcome;

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
        buttonForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(LoginActivity.this, ResetPasswordActivity.class);
                startActivity(intent1);
            }
        });

        loginViewModel.loginResponse.observe(this, (resp) -> {
            if (resp.getSuccess()) {
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                intent.putExtra("USERNAME", loginViewModel.user.getUsername());
                Toast.makeText(this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Đăng nhập thất bại", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
