package com.huce.hca.presentation.app.review.dto;

public class ReviewBookRequestDTO {
    private String description;
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
